def import_puzzle_input(): # pragma: no cover
    with open('day_1_part_1.txt', 'r') as file: 
        masses = file.readlines()
    return [int(e) for e in masses]

def fuel(mass):
    return mass // 3 - 2

if __name__ == '__main__':  # pragma: no cover
    total = 0
    for mass in import_puzzle_input():
        total += fuel(mass)
    print(total)